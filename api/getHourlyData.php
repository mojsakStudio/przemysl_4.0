<?php
header('Content-Type: application/json');

$now = date('Y/m/d');
$content = file('../data/trend/' . $now . '/hour.txt', FILE_IGNORE_NEW_LINES);

$labels = array();
$data = array();
unset($content[0]);
foreach ($content as $index => $line){
    $columns = explode(';', $line);
    array_push($labels,  $columns[0]);
    array_push($data,  $columns[1]);
}

$response = [];
$response['labels'] = $labels;
$response['data'] = $data;
echo json_encode($response);
