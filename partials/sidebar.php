<nav id="sidebar" class="sidebar">
    <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand" href="#index">
            <span class="align-middle">Przemysł 10.0</span>
        </a>

        <ul class="sidebar-nav">
			<!--  -->
            <li class="sidebar-header">
                Raporty
            </li>

            <li class="sidebar-item active">
                <a class="sidebar-link" href="#index">
                    <i class="align-middle" data-feather="home"></i> <span class="align-middle">Kokpit</span>
                </a>
            </li>

            <li class="sidebar-item">
                <a class="sidebar-link" href="#pages-profile">
                    <i class="align-middle" data-feather="activity"></i> <span class="align-middle">Raport dzienny</span>
                </a>
            </li>

            <li class="sidebar-item">
                <a class="sidebar-link" href="#pages-invoice">
                    <i class="align-middle" data-feather="bar-chart"></i> <span class="align-middle">Raport okresowy</span>
                </a>
            </li>

            <li class="sidebar-item">
                <a class="sidebar-link" href="#pages-blank">
                    <i class="align-middle" data-feather="pie-chart"></i> <span class="align-middle">Wydajność</span>
                </a>
			</li>

			<!--  -->

			<li class="sidebar-header">
                Zasoby
            </li>
            <li class="sidebar-item">
                <a class="sidebar-link" href="#pages-invoice">
                    <i class="align-middle" data-feather="users"></i> <span class="align-middle">Pracownicy</span>
                </a>
            </li>

            <li class="sidebar-item">
                <a class="sidebar-link" href="#pages-blank">
                    <i class="align-middle" data-feather="hard-drive"></i> <span class="align-middle">Maszyny</span>
                </a>
			</li>

			<li class="sidebar-item">
                <a class="sidebar-link" href="#pages-invoice">
                    <i class="align-middle" data-feather="box"></i> <span class="align-middle">Materiały</span>
                </a>
            </li>
			
			<!--  -->

            <li class="sidebar-header">
                Moduły
            </li>
            <li class="sidebar-item">
                <a data-target="#ui" data-toggle="collapse" class="sidebar-link collapsed">
                    <i class="align-middle" data-feather="clock"></i> <span class="align-middle">RCP</span>
                </a>
                <ul id="ui" class="sidebar-dropdown list-unstyled collapse " data-parent="#sidebar">
                    <li class="sidebar-item"><a class="sidebar-link" href="#ui-alerts">Czas pracy</a></li>
                    <li class="sidebar-item"><a class="sidebar-link" href="#ui-buttons">Wydajność</a></li>
                    <li class="sidebar-item"><a class="sidebar-link" href="#ui-general">Cuda</a></li>
                    <li class="sidebar-item"><a class="sidebar-link" href="#ui-grid">Wianki</a></li>
                </ul>
            </li>

            <!--  -->

            <li class="sidebar-header">
                Konfiguracja
            </li>
			
			<li class="sidebar-item">
                <a class="sidebar-link" href="#pages-settings">
                    <i class="align-middle" data-feather="settings"></i> <span class="align-middle">Ustawienia</span>
                </a>
            </li>

			<!--  -->
        </ul>

        <div class="sidebar-cta">
            <div class="sidebar-cta-content">
                <strong class="d-inline-block mb-2">Pakiet DARMOCHA <i class="align-middle" data-feather="award"></i></strong>
                <div class="mb-3 text-sm">
                    Chcesz wyciągnąć jeszcze więcej z naszego systemu?
                </div>
				<a href="#pricing" target="_blank" class="btn btn-primary btn-block">Wykup pakiet PRO</a>
            </div>
        </div>
    </div>
</nav>