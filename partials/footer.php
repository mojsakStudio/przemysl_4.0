<footer class="footer">
    <div class="container-fluid">
        <div class="row text-muted">
            <div class="col-6 text-left">
                <p class="mb-0">
                    <a href="https://mojsak-studio.pl" class="text-muted">Stworzone przez <strong>mojsakStudio</strong></a> &copy;
                </p>
            </div>
            <div class="col-6 text-right">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a class="text-muted" href="#">Zgłoś błąd</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="text-muted" href="#">Dokumentacja</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="text-muted" href="#">Polityka prywatności</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="text-muted" href="#">Regulamin</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>