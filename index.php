<!DOCTYPE html>
<html lang="en">

<head>
	<?php include('partials/head.php') ?>
	<title>Przemysł 10.0</title>
</head>

<body>
	<div class="wrapper">
        <?php include('partials/sidebar.php') ?>

		<div class="main">
            <?php include('partials/navbar.php') ?>

			<main class="content">
				<div class="container-fluid p-0">

					<div class="row mb-2 mb-xl-3">
						<div class="col-auto d-none d-sm-block">
							<h3>Linia <strong class="reference-name"></strong></h3>
						</div>

						<div class="col-auto ml-auto text-right mt-n1">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb bg-transparent p-0 mt-1 mb-0">
									<li class="breadcrumb-item"><a href="#">Kokpit</a></li>
									<li class="breadcrumb-item"><a href="#">Linie</a></li>
									<li class="breadcrumb-item active reference-name" aria-current="page">Opakowania</li>
								</ol>
							</nav>
						</div>
					</div>
					<div class="row">
						<div class="col-xl-6 col-xxl-5 d-flex">
							<div class="w-100">
								<div class="row">
									<div class="col-sm-6">
										<div class="card">
											<div class="card-body">
												<h5 class="card-title mb-4">Chwilowa liczba sztuk</h5>
												<h1 class="mt-1 mb-1">
                                                    <div class="mb-1">
                                                        <div class="mini-graph">
                                                            <div class="mini-graph-bars">
                                                                <div class="bar bar-1" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-2" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-3" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-4" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-5" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-6" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>
                                                            </div>
                                                        </div>
                                                        <span id="current_ok"></span>
                                                        <button id="reset" class="btn btn-primary float-right">Resetuj</button>
                                                    </div>
                                                </h1>
                                            </div>
										</div>
										<div class="card">
											<div class="card-body">
                                                <h5 class="card-title mb-4">Liczba sztuk na godzinę</h5>
                                                <h1 class="mt-1 mb-1">
                                                    <div class="mb-1">
                                                        <div class="mini-graph">
                                                            <div class="mini-graph-bars">
                                                                <div class="bar bar-1" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-2" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-3" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-4" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-5" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-6" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>
                                                            </div>
                                                        </div>
                                                        <span id="hour_ok"></span>
                                                    </div>
                                                </h1>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="card">
											<div class="card-body">
                                                <h5 class="card-title mb-4">Liczba sztuk na zmianę</h5>
                                                <h1 class="mt-1 mb-1">
                                                    <div class="mb-1">
                                                        <div class="mini-graph">
                                                            <div class="mini-graph-bars">
                                                                <div class="bar bar-1" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-2" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-3" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-4" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-5" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-6" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>
                                                            </div>
                                                        </div>
                                                        <span id="shift_ok"></span>
                                                    </div>
                                                </h1>
											</div>
										</div>
										<div class="card">
											<div class="card-body">
                                                <h5 class="card-title mb-4">Liczba sztuk na godzinę</h5>
                                                <h1 class="mt-1 mb-1">
                                                    <div class="mb-1">
                                                        <div class="mini-graph">
                                                            <div class="mini-graph-bars">
                                                                <div class="bar bar-1" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-2" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-3" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-4" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-5" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>

                                                                <div class="bar bar-6" style="height: 100%;"><div class="bar bar-inner" style="height: 0%;"></div></div>
                                                            </div>
                                                        </div>
                                                        <span id="day_ok"></span>
                                                    </div>
                                                </h1>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-xl-6 col-xxl-7">
							<div class="card flex-fill w-100">
								<div class="card-header">

									<h5 class="card-title mb-0">Wydajność na godzinę</h5>
								</div>
								<div class="card-body py-3">
									<div class="chart chart-sm">
										<canvas id="chartjs-dashboard-line"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-12 col-lg-8 col-xxl-9 d-flex">
							<div class="card flex-fill">
								<div class="card-header">
									<h5 class="card-title mb-0">Wydajność operatorów</h5>
								</div>
								<table class="table table-hover my-0">
									<thead>
										<tr>
											<th>Operator</th>
											<th class="d-none d-xl-table-cell">Zmiana</th>
											<th class="d-none d-xl-table-cell">Liczba sztuk</th>
										</tr>
									</thead>
									<tbody id="shift_data">
                                        <tr>
                                            <td>0001</td>
                                            <td class="d-none d-xl-table-cell">1</td>
                                            <td class="d-none d-xl-table-cell">0</td>
                                        </tr>
                                        <tr>
                                            <td>0002</td>
                                            <td class="d-none d-xl-table-cell">2</td>
                                            <td class="d-none d-xl-table-cell">0</td>
                                        </tr>
                                        <tr>
                                            <td>0003</td>
                                            <td class="d-none d-xl-table-cell">3</td>
                                            <td class="d-none d-xl-table-cell">0</td>
                                        </tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-12 col-md-6 col-xxl-3 d-flex order-2 order-xxl-3">
							<div class="card flex-fill w-100">
								<div class="card-header">
									<h5 class="card-title mb-0">Stan maszyny</h5>
								</div>
<!--                                TODO: Przełącznik do zmiany widoku na wykres liniowy-->
								<div class="card-body d-flex">
									<div class="align-self-center w-100">
										<div class="py-3">
											<div class="chart chart-xs">
												<canvas id="chartjs-dashboard-pie"></canvas>
											</div>
										</div>

										<table class="table mb-0">
											<tbody>
												<tr>
													<td>Włączona</td>
													<td class="text-right">73%</td>
												</tr>
												<tr>
													<td>Wyłączona</td>
													<td class="text-right">27%</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</main>

            <?php include('partials/footer.php') ?>

		</div>
	</div>

    <?php include('partials/scripts.php') ?>
</body>
</html>