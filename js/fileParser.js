function parseDataFromFile(){
    $.ajax({
        url: baseUrl + '/getCurrentData.php',
        type: "POST",
        success: function (response) {
            $(document).trigger('date', [response[0]]);
            $(document).trigger('reference', [response[1]]);
            $(document).trigger('current_ok', [response[2]]);
            $(document).trigger('shift_ok', [response[3]]);
            $(document).trigger('day_ok', [response[4]]);
            $(document).trigger('hour_ok', [response[5]]);
            $(document).trigger('current_not_ok', [response[6]]);
            $(document).trigger('shift_not_ok', [response[7]]);
            $(document).trigger('day_not_ok', [response[8]]);
            $(document).trigger('hour_not_ok', [response[9]]);
        }
    })
}

function parseHourlyData(){
    $.ajax({
        url: baseUrl + '/getHourlyData.php',
        type: "POST",
        success: function (response) {
            drawHourlyWidget(response.labels, response.data);
        }
    })
}


jQuery(document).ready(function() {
    parseDataFromFile();
    parseHourlyData();

    setInterval(function (){
        parseDataFromFile();
    },  1000);
    setInterval(function (){
        parseHourlyData();
    },  1000 * 60 * 60);
});