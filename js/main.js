var baseUrl = 'api';

$(document).on('current_ok', null, function(event, value) {
    updatePanelField('#current_ok', value, 0);
    setColorByValue('#current_ok', value, chwilowy_min, chwilowy_max, false);
    updateGraph('#current_ok', value, chwilowy_min, chwilowy_max, false);
});

$(document).on('day_ok', null, function(event, value) {
    updatePanelField('#day_ok', value, 0);
    setColorByValue('#day_ok', value, chwilowy_min, chwilowy_max, false);
    updateGraph('#day_ok', value, chwilowy_min, chwilowy_max, false);
});

$(document).on('shift_ok', null, function(event, value) {
    updatePanelField('#shift_ok', value, 0);
    setColorByValue('#shift_ok', value, chwilowy_min, chwilowy_max, false);
    updateGraph('#shift_ok', value, chwilowy_min, chwilowy_max, false);
});

$(document).on('hour_ok', null, function(event, value) {
    updatePanelField('#hour_ok', value, 0);
    setColorByValue('#hour_ok', value, chwilowy_min, chwilowy_max, false);
    updateGraph('#hour_ok', value, chwilowy_min, chwilowy_max, false);
});


$(document).on('reference', null, function(event, value) {
    $('.reference-name').text(value);
});

$(document).on('done', null, function(event, value) {
    $('#done').text(value === '-1'? 'TAK' : 'NIE');
});

$(document).on('date', null, function(event, value) {
    var date = value.split('_')[1];
    $('#date').text(date);
});

$(document).on('click', '#reset', function () {
    $.ajax({
        url: baseUrl + '/setConfig.php',
        type: "POST",
        success: function (response) {
            alert('zresetowano');
        }
    })
});

function updatePanelField(field, value, fixed) {
    if (value && !isNaN(value)) {
        value = parseInt(value).toFixed(fixed);
    } else {
        value = (value) ? value : 0;
    }
    $(field).text(value);
}


//TODO: Osobny plik utils na takie funckje
function setColorByValue(field, value, min, max, bg) {
    value = value ? parseInt(value) : 0;

    var max10 = max - (max / 100 * warningTolerance);
    var min10 = min + (min / 20 * warningTolerance);
    //Sprawdzamy czy wartość zbliża się do maxa
    if (max != null && value > max) {
        setColor(field, 'red', bg);
    } else if (max != null && value > max10) {
        setColor(field, 'orange', bg);
    } else if (min != null && value < min) {
        setColor(field, 'red', bg);
    } else if (min != null && value < min10) {
        setColor(field, 'orange', bg);
    } else {
        setColor(field, 'green', bg);
    }
}

function setColor(field, color, bg) {
    var addon = (bg) ? 'bg-' : '';
    $(field).parent().removeClass(addon + 'green');
    $(field).parent().removeClass(addon + 'red');
    $(field).parent().removeClass(addon + 'orange');
    $(field).parent().addClass(addon + color);
}


function updateGraph(field, value, min, max) {
    var graph = $(field).parent().parent().find('.mini-graph-bars');
    var first = graph.find('.bar:not(.bar-inner)').first();
    var last = graph.find('.bar:not(.bar-inner)').last();
    first.remove();
    var html = last.clone();
    html.insertAfter(last);
    last = graph.find('.bar:not(.bar-inner)').last();
    var bar = last.find('.bar-inner');
    if(value > max)value = max;

    var maxPercent = (max) ? max : min * 10;
    var percent = ((value - min) * 100) / (maxPercent - min); //Jakim procentem jest liczba pomiędzy w przedziale min, max
    bar.height(percent + '%');

    //TODO: Wyeksportować do wspólnej funkcji
    var max10 = (max) ? max - (max / 100 * warningTolerance) : 0;
    var min10 = (min) ? min + (min / 100 * warningTolerance) : 0;
    bar.removeClass('bg-red');
    bar.removeClass('bg-green');
    bar.removeClass('bg-orange');
    if (max != null && value > max) {
        bar.addClass('bg-red');
    } else if (max != null && value > max10) {
        bar.addClass('bg-orange');
    } else if (min != null && value < min) {
        bar.addClass('bg-red');
    } else if (min != null && value < min10) {
        bar.addClass('bg-orange');
    } else {
        bar.addClass('bg-green');
    }
}

function drawHourlyWidget(labels, data) {
    var ctx = document.getElementById("chartjs-dashboard-line").getContext("2d");
    var gradient = ctx.createLinearGradient(0, 0, 0, 225);
    gradient.addColorStop(0, "rgba(215, 227, 244, 1)");
    gradient.addColorStop(1, "rgba(215, 227, 244, 0)");
    // Line chart
    new Chart(document.getElementById("chartjs-dashboard-line"), {
        type: "line",
        data: {
            labels: labels,
            datasets: [{
                label: "Wyprodukowanych sztuk",
                fill: true,
                backgroundColor: gradient,
                borderColor: window.theme.primary,
                data: data
            }]
        },
        options: {
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            tooltips: {
                intersect: false
            },
            hover: {
                intersect: true
            },
            plugins: {
                filler: {
                    propagate: false
                }
            },
            scales: {
                xAxes: [{
                    reverse: true,
                    gridLines: {
                        color: "rgba(0,0,0,0.0)"
                    }
                }],
                yAxes: [{
                    ticks: {
                        stepSize: 1000
                    },
                    display: true,
                    borderDash: [3, 3],
                    gridLines: {
                        color: "rgba(0,0,0,0.0)"
                    }
                }]
            }
        }
    });
}